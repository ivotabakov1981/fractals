﻿using System;

namespace EulerProject
{
    /// <summary>
    /// The number, 197, is called a circular prime because all rotations 
    /// of the digits: 197, 971, and 719, are themselves prime.
    /// There are thirteen such primes below 100:
    /// 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.
    /// How many circular primes are there below one million?
    /// </summary>
    class Problem35
    {
        static void Main(string[] args)
        {
            Solver s = new Solver();
            int counter = 0;
            for (int i = 2; i < 1000000; i++)
            {
                if (i % 2 != 0 && i % 3 != 0 && i % 5 != 0 && s.IsCircularPrime(i))
                    counter++;
            }
            //counting 2 , 3, 5
            counter = counter + 3;
        }
    }
}
